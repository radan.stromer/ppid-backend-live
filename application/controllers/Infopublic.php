<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Infopublic extends CI_Controller {
    function  __construct(){
        parent::__construct();
        if(empty($_SESSION['admin_id']) AND $_SESSION['admin_level']!=1){
            redirect('/login');
		}
		$this->load->model('M_infopublic');
	}
	
	public function sub_list($category)
	{
		$data['category_info'] = $category;
		switch ($category) {
			case '2':
				$category_db = 'serta merta';
				break;
			case '3':
				$category_db = 'setiap saat';
				break;
			case '4':
				$category_db = 'laporan';
				break;
			case '5':
				$category_db = 'peraturan';
			break;
			default:
				$category_db = 'berkala';
				break;
		}
	    $data['admin'] = $this->m_crud->get_list_one_where('info_public_sub','category_info',$category_db,'id','asc');
		$this->load->view('public/sub_list',$data);
	}

	public function sub_form($category,$id=''){
		switch ($category) {
			case '2':
				$data['category_info'] = 'serta merta';
				break;
			case '3':
				$data['category_info'] = 'setiap saat';
				break;
			case '4':
				$data['category_info'] = 'laporan';
				break;
			case '5':
				$data['category_info'] = 'peraturan';
				break;
			default:
				$data['category_info'] = 'berkala';
				break;
		}

	    if($this->input->post()){
			if(!empty($this->input->post('id'))) $this->M_infopublic->sub_edit($this->input->post());
			else $this->M_infopublic->sub_add($this->input->post());
			redirect('Infopublic/sub_list/'.$category);
		}

		if(!empty($id)){
			$data['id'] = $id;
			$data = $this->M_infopublic->sub_get($id);
		}
		$this->load->view('public/sub_form',$data);
	}
	
	public function sub_hapus($id){
		$r = $this->M_infopublic->sub_delete($id);
	    if($r) redirect('Infopublic/sub_list/1');
	}

	public function item_list($category_info_id='')
	{
		$data['sub_id'] = $category_info_id;
		switch ($category_info_id) {
			case '2':
				$category_info = 'serta merta';
				break;
			case '3':
				$category_info = 'setiap saat';
				break;
			case '4':
				$category_info = 'laporan';
				break;
			case '5':
				$category_info = 'peraturan';
				break;
			default:
				$category_info = 'berkala';
				break;
		}
		$data['data'] = $this->M_infopublic->get_item_list($category_info);
		$this->load->view('public/item_list',$data);
	}

	public function item_form($category_info_id,$id=''){
		
		switch ($category_info_id) {
			case '2':
				$category_info = 'serta merta';
				break;
			case '3':
				$category_info = 'setiap saat';
				break;
			case '4':
				$category_info = 'laporan';
				break;
			case '5':
				$category_info = 'peraturan';
				break;
			default:
				$category_info = 'berkala';
				break;
		}

	    if($this->input->post()){
			$config['upload_path']          = './assets/upload/infopublic/';
			$config['allowed_types']        = 'gif|jpg|png|pdf';
			//$config['max_size']             = 2048;

			$this->load->helper('security');
			$ext = pathinfo($_FILES['file_dokumen']['name'], PATHINFO_EXTENSION);
			$file_name = do_hash($_FILES['file_dokumen']['name'].date('Y-m-d H:i:s'), 'md5').'.'.$ext;
			$config['file_name'] = $file_name;

			$this->load->library('upload', $config);

			if ( ! $this->upload->do_upload('file_dokumen')){
				$error = array('error' => $this->upload->display_errors());
				if(empty($id))pre($error);
			}
				
			if(!empty($this->input->post('id'))) $this->M_infopublic->item_edit($this->input->post(),$file_name);
			else $this->M_infopublic->item_add($this->input->post(),$file_name);
			redirect('Infopublic/item_list/'.$category_info_id);
		}

		if(!empty($id)){
			$data['id'] = $id;
			$data = $this->M_infopublic->item_get($id);
		}
	
		$data['sub'] = $this->m_crud->get_list_one_where('info_public_sub','category_info',$category_info,'id','asc');
		//pre($data);
		$data['red'] = $category_info_id;
	
		$this->load->view('public/item_form',$data);
	}
	
	public function item_hapus($id,$category_info_id=''){
		$category_info = $this->M_infopublic->item_delete($id);
		//$this->session->set_flashdata('msg','data berhasil dihapus');
	    redirect('Infopublic/item_list/'.$category_info_id);
	}

	public function item_file_hapus($id,$category_info_id){
		$category_info = $this->M_infopublic->item_file_delete($id);
		//$this->session->set_flashdata('msg','data berhasil dihapus');
	    redirect('Infopublic/item_form/'.$category_info_id.'/'.$id);
	}
}

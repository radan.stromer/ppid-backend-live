<?php
    require APPPATH . 'third_party/RestController.php';
    require APPPATH . 'third_party/Format.php';

    use chriskacerguis\RestServer\RestController;

    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET");
    
    class Infopublic extends RestController {
        function  __construct(){
            parent::__construct();
            // Load these helper to create JWT tokens
            $this->load->helper(['jwt', 'authorization']);
        }

        public function info_public_get($category){
            switch ($category) {
                case '2':
                    $category_info = 'serta merta';
                    break;
                case '3':
                    $category_info = 'setiap saat';
                    break;
                case '4':
                    $category_info = 'laporan';
                    break;
                case '5':
                    $category_info = 'peraturan';
                    break;
                default:
                    $category_info = 'berkala';
                    break;
            }

            $pengajuan['sub'] = $this->m_crud->get_list_one_where('info_public_sub','category_info',$category_info,'id','desc');
            $alphabet = range('A', 'Z');

            foreach ($pengajuan['sub']as $key => $v) {
                $pengajuan['sub'][$key]['item'] = $this->m_crud->get_list_one_where('info_public_item','sub_id',$v['id'],'id','desc');
                $pengajuan['sub'][$key]['urutan_abjad'] = $alphabet[$key];
            }
           
            $status = parent::HTTP_OK;
            $response = ['status' => $status, 'data' => $pengajuan['sub']];
            $this->response($response, $status);
        }

    }
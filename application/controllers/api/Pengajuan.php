<?php
    require APPPATH . 'third_party/RestController.php';
    require APPPATH . 'third_party/Format.php';

    use chriskacerguis\RestServer\RestController;

    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET, POST, PATCH, PUT");
    
    class Pengajuan extends RestController {
        function  __construct(){
            parent::__construct();
            // Load these helper to create JWT tokens
            $this->load->helper(['jwt', 'authorization']);
        }

        public function pengajuan_get($id_pengajuan=''){
            $user = $this->verify_request();
            $user_id = $this->m_crud->get_row('users','email',$user->user_email);

            if(!empty($id_pengajuan)){
                $this->load->model('M_pengajuan');
                $pengajuan = $this->M_pengajuan->get_detail($id_pengajuan);
            }else{
                $pengajuan = $this->m_crud->get_list_one_where('pengajuan','user_id',$user_id['id'],'id','desc');
                foreach ($pengajuan as $key => $value) {
                    if($value['status']=='sedang direview'){
                        $pengajuan[$key]['status'] = 'Permohonan sedang diproses';
                    }
                }
            }
            //$pengajuan['waktu'] = strtotime($pengajuan['waktu']);
            // $pengajuan['waktu'] = date('d m Y H:i', $pengajuan['waktu']);   
            // Prepare the response
            $status = parent::HTTP_OK;
            $response = ['status' => $status, 'pengajuan' => $pengajuan];
            $this->response($response, $status);
        }

        public function pengajuan_patch(){
            $user = $this->verify_request();
            $user_data = $this->m_crud->get_row('users','email',$user->user_email);

            $post = $this->patch();

            $this->load->model('M_pengajuan');
            $pengajuan = $this->M_pengajuan->update_selesai($post['id']);
            if($pengajuan){
                $status = parent::HTTP_OK;
                $response = ['status' => $status, 'msg' => 'pengajuan berhasil di update'];
                $this->response($response, $status);
            }else{
                $this->response( [
                    'status' => false,
                    'message' => 'data gagal diupdate'
                ], 404 );
            }
        }

        public function pengajuan_post(){
            $user = $this->verify_request();
            $user_data = $this->m_crud->get_row('users','email',$user->user_email);
            
            $post = $this->post();
            $post['user_id'] = $user_data['id'];
            
            //get no pengajuan
            $this->load->model('M_pengajuan');
            $post['no_pengajuan'] = $this->M_pengajuan->get_last_no_pengajuan();

            $add = $this->m_crud->add_return_id('pengajuan',$post);
            if($add){
                $status = parent::HTTP_OK;
                $response = ['status' => $status, 'id_pengajuan' => $add ];
            }
            
            $this->response($response, $status);
        }

        public function uploadktp_post(){
            
            $user = $this->verify_request();
            $config['upload_path']          = 'assets/upload/ktp';
            $config['allowed_types']        = 'gif|jpg|png|pdf';
            $config['max_size']             = 10000;
            $this->load->library('upload', $config);

            if($this->upload->do_upload('ktp')) {
                $status = parent::HTTP_OK;
                $response = ['status' => $status, 'hasil' => 'file ktp berhasil di upload' ];
                $data = array('file_ktp' => $_FILES['ktp']['name']);
                $explode = explode('-',$_FILES['ktp']['name']);
                $id = $explode[1];
                $this->m_crud->edit('pengajuan',$data,'id',$id);
                $this->response($response, $status);
                exit;
            }else{
                $status = parent::HTTP_OK;
                $response = ['status' => $status, 'hasil' => $this->upload->display_errors() ];
                $this->response($response, $status);
                exit;
            }
        }

        public function uploaddoc_post(){
            $user = $this->verify_request();
            $config['upload_path']          = './assets/upload/doc';
            $config['allowed_types']        = 'gif|jpg|png|pdf';
            $config['max_size']             = 2000;
            $this->load->library('upload', $config);

            if($this->upload->do_upload('doc')) {
                $explode = explode('-',$_FILES['doc']['name']);
                $id = $explode[1];
                $data = array('file_dokumen' => $_FILES['doc']['name']);
                $this->m_crud->edit('pengajuan',$data,'id',$id);

                $status = parent::HTTP_OK;
                $response = ['status' => $status, 'hasil' => 'file dokumen berhasil di upload' ];
                $this->response($response, $status);
                exit;
            }else{
                $status = parent::HTTP_OK;
                $response = ['status' => $status, 'hasil' => $this->upload->display_errors() ];
                $this->response($response, $status);
                exit;
            }
        } 

        private function verify_request(){
            $headers = $this->input->request_headers();
            $token   = $headers['Authorization']; 
            try {
                // Validate the token
                // Successfull validation will return the decoded user data else returns false
                $data = AUTHORIZATION::validateToken($token);
                if ($data === false) {
                    $status = parent::HTTP_UNAUTHORIZED;
                    $response = ['status' => $status, 'msg' => 'Unauthorized Access!'];
                    $this->response($response, $status);
                    exit();
                } else {
                    return $data;
                }
            } catch (Exception $e) {
                // Token is invalid
                // Send the unathorized access message
                $status = parent::HTTP_UNAUTHORIZED;
                $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
                $this->response($response, $status);
            }
        }
    }
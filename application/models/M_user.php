<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class M_user extends CI_Model
{ 
    function cek_email_exist($email){
       return $this->db->query("SELECT * FROM users WHERE email=?",array($email))->num_rows();
    }
    
    function insert_user($data){
        $options = [
            'cost' => 10,
        ];
 
        $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT, $options);
        return $this->db->insert('users',$data);
    }

    function cek_login($data){
        $query = $this->db->select('id,nama,email,password')
                          ->where('email',$data['email'])
                          ->where('status','1')
                          ->get('users')->row_array();
        if(password_verify($data['password'], $query['password'])) return $query;
        else return false;
    }

    function cek_login_admin($email,$pass){
        $sql = "SELECT * FROM admin WHERE email = ? AND pass = ?";
        return $this->db->query($sql, array($email, $pass))->row_array(); 
    }
}
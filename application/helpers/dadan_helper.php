<?php 

if ( ! function_exists('pre'))
{
    function pre($data,$exit=true)
    {
        echo "<pre>";
        print_r($data);
        if($exit==true) exit;
    }   
}

if ( ! function_exists('batas_waktu'))
{
    function batas_waktu($data)
    {
		$date = explode(' ',$data);
		$Date = $date[0];
		$date2 = date('Y-m-d', strtotime($Date. ' + 14 days'));
		$diff = strtotime($date2) - strtotime(date('Y-m-d')); 
		echo '<span style="width:100%; display:block; margin-top:7px; text-align:center">'.abs(round($diff / 86400)).' Hari</span>';
    }   
}

if ( ! function_exists('batas_waktu_keberatan'))
{
    function batas_waktu_keberatan($data)
    {
		$date = explode(' ',$data);
		$Date = $date[0];
		$date2 = date('Y-m-d', strtotime($Date. ' + 7 days'));
		$diff = strtotime($date2) - strtotime(date('Y-m-d')); 
		echo '<span style="width:100%; display:block; margin-top:7px; text-align:center">'.abs(round($diff / 86400)).' Hari</span>';
    }   
}

if ( ! function_exists('cek_notif_baru'))
{
    function cek_notif_baru()
    {
		$CI =& get_instance();
		$r = $CI->db->select('*')
			   ->where('status','sedang direview')
			   ->get('pengajuan')
			   ->result_array();
		//pre($r);
		
		foreach ($r as $key => $v) {
			echo "<a class='dropdown-item d-flex align-items-center' href='".base_url('pengajuan/detail/'.$v['id'])."'>";
			echo $v['no_pengajuan'];
			echo "</a>";
		}
    }   
}

if ( ! function_exists('cek_jumlah'))
{
    function cek_jumlah()
    {
		$CI =& get_instance();
		$r = $CI->db->select('*')
			   ->where('status','sedang direview')
			   ->get('pengajuan')
			   ->result_array();
		echo count($r);
    }   
}

if ( ! function_exists('integerToRoman'))
{
	function integerToRoman($integer)
	{
	// Convert the integer into an integer (just to make sure)
	$integer = intval($integer);
	$result = '';
	
	// Create a lookup array that contains all of the Roman numerals.
	$lookup = array('M' => 1000,
	'CM' => 900,
	'D' => 500,
	'CD' => 400,
	'C' => 100,
	'XC' => 90,
	'L' => 50,
	'XL' => 40,
	'X' => 10,
	'IX' => 9,
	'V' => 5,
	'IV' => 4,
	'I' => 1);
	
	foreach($lookup as $roman => $value){
	// Determine the number of matches
	$matches = intval($integer/$value);
	
	// Add the same number of characters to the string
	$result .= str_repeat($roman,$matches);
	
	// Set the integer to be the remainder of the integer and the value
	$integer = $integer % $value;
	}
	
	// The Roman numeral should be built, return it
	return $result;
	}
}
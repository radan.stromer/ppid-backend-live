<?php $this->load->view('include/header');?>
        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Daftar Sub bagian</h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-body">
              <div class="table-responsive">
                <a href="<?=base_url('Infopublic/sub_form/'.$category_info);?>" class="btn btn-info">+ Tambah sub bagian</a>
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Nama sub bagian</th>
                      <th>Kategori</th>
                      <th>Tindakan</th>
                    </tr>
                  </thead>
                  <?php foreach($admin as $v){?>
                    <tr>
                      <td><?=$v['title'];?></td>
                      <td><?=$v['category_info'];?></td>
                      <td><a style="padding:5px; font-size:10px;" class="btn btn-primary" href="<?=base_url('Infopublic/sub_form/'.$category_info.'/'.$v['id']);?>">Edit</a>
                      <a onclick="return confirm('Anda yakin ingin menghapus?')" class="btn btn-danger btn-hapus btn-sm btn-circle" href="<?=base_url('Infopublic/sub_hapus/'.$v['id']);?>"><i class="fas fa-trash"></a></td>
                    </tr>
                  <?php }?>
                  <tfoot>
                    <tr>
                      <th>Nama sub bagian</th>
                      <th>Kategori</th>
                      <th>Tindakan</th>
                    </tr>
                  </tfoot>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->


<?php $this->load->view('include/footer');?>


<?php $this->load->view('include/header');?>
<div class="card shadow mb-4">
    <div class="card-body">
        <h3><?=(!empty($id))?'Edit':'Tambah ';?> Sub bagian</h3>
        <form method="post">
            <?php $csrf = array(
                'name' => $this->security->get_csrf_token_name(),
                'hash' => $this->security->get_csrf_hash()
            );
            ?>
            <?php if(!empty($id)){?>
                <input type="hidden" name="id" value="<?=$id;?>">
            <?php }?>
            <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
            <input type="hidden" name="category_info" value="<?=$category_info;?>">
            <div class="form-group">
                <label>Title</label>
                <input type="text" name="title" value="<?=(!empty($title))?$title:'';?>" class="form-control"/>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
<?php $this->load->view('include/footer');?>
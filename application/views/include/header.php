<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=gb18030">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>PPID Dashboard</title>
  <link rel="stylesheet" type="text/css" href="<?=base_url('assets/');?>DataTables/datatables.min.css"/>

  <!-- Custom fonts for this template-->
  <link href="<?=base_url('assets/');?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?=base_url('assets/');?>css/sb-admin-2.css?v=<?=date('ymdHis');?>" rel="stylesheet">
   <link href="<?=base_url('assets/');?>css/ppid.css?v=8" rel="stylesheet">

</head>

<body id="page-top">
  <!-- Page Wrapper -->
  <div id="wrapper">
    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
      <!-- Sidebar - Brand -->
      <a style="background:white;" class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
      <img id="kop-ppid" class="img-fluid" src="<?=base_url('assets/img/kopppid2.png');?>" alt="">
        <!-- <img src="<?=base_url('assets/img/066b2a56-3e08-4ef8-a5eb-8554848f9b2c.jpeg');?>" class="img-fluid" style="width:36%;" alt=""> -->
        <!-- <div class="sidebar-brand-text mx-3">PPID BNP2TKI</div> -->
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="index.html">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Pengajuan
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="<?=base_url('pengajuan');?>" aria-expanded="true">
          <i class="fas fa-fw fa-cog"></i>
          <span>Daftar Pengajuan</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link collapsed" href="<?=base_url('pengajuan/keberatan');?>" aria-expanded="true">
          <i class="fas fa-fw fa-cog"></i>
          <span>Daftar Keberatan</span>
        </a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">


      <?php if($_SESSION['admin_level']==1){?>
      <!-- Heading -->
      <div class="sidebar-heading">
        Akses Manajemen
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-fw fa-folder"></i>
          <span>Manajemen admin</span>
        </a>
        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
           
            <a class="collapse-item" href="<?=base_url('admin');?>">Lihat daftar Admin</a>
            <a class="collapse-item" href="<?=base_url('admin/add');?>">Tambah Admn</a>
           
          </div>
        </div>
      </li>
    <?php }?>

     <!-- Divider -->
     <hr class="sidebar-divider">


      <!-- Heading -->
      <div class="sidebar-heading">
        Pengaturan konten Aplikasi Android
      </div>

      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#berkala" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-fw fa-folder"></i>
          <span>Info berkala</span>
        </a>
        <div id="berkala" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="<?=base_url('Infopublic/sub_list/1');?>">Atur sub-bagian</a>
            <a class="collapse-item" href="<?=base_url('Infopublic/item_list/1');?>">Atur item</a>
          </div>
        </div>
      </li>


      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#serta-merta" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-fw fa-folder"></i>
          <span>Info serta merta</span>
        </a>
        <div id="serta-merta" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
          <a class="collapse-item" href="<?=base_url('Infopublic/sub_list/2');?>">Atur sub-bagian</a>
            <a class="collapse-item" href="<?=base_url('Infopublic/item_list/2');?>">Atur item</a>
          </div>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#tersedia-setiap-saat" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-fw fa-folder"></i>
          <span>Info tersedia setiap saat</span>
        </a>
        <div id="tersedia-setiap-saat" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="<?=base_url('Infopublic/sub_list/3');?>">Atur sub-bagian</a>
            <a class="collapse-item" href="<?=base_url('Infopublic/item_list/3');?>">Atur item</a>
          </div>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#laporan" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-fw fa-folder"></i>
          <span>Laporan</span>
        </a>
        <div id="laporan" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
          <a class="collapse-item" href="<?=base_url('Infopublic/sub_list/4');?>">Atur sub-bagian</a>
            <a class="collapse-item" href="<?=base_url('Infopublic/item_list/4');?>">Atur item</a>
          </div>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#peraturan" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-fw fa-folder"></i>
          <span>Peraturan</span>
        </a>
        <div id="peraturan" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
          <a class="collapse-item" href="<?=base_url('Infopublic/sub_list/5');?>">Atur sub-bagian</a>
            <a class="collapse-item" href="<?=base_url('Infopublic/item_list/5');?>">Atur item</a>
          </div>
        </div>
      </li>
     
    </ul>
    <!-- End of Sidebar -->
 <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
         
          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

        

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>

            <!-- Nav Item - Alerts -->
            <li class="nav-item dropdown no-arrow mx-1">
              <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-bell fa-fw"></i>
                <span class="badge badge-danger badge-counter"><?=cek_jumlah();?></span>
              </a>
              <!-- Dropdown - Alerts -->
              <div id="notif-pengajuan" class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
                <h6 class="dropdown-header">
                  Pengajuan Baru
                </h6>
                <?=cek_notif_baru();?>
              </div>
            </li>

            

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?=$_SESSION['admin_name'];?></span>
                <img class="img-profile rounded-circle" src="<?=base_url('assets/img/066b2a56-3e08-4ef8-a5eb-8554848f9b2c.jpeg');?>">
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="#">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Profile
                </a>
               
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="<?=base_url('login/logout');?>">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

<?php $this->load->view('include/header');?>
<div class="card shadow mb-4">
    <div class="card-body">
        <h3>Tambah admin</h3>
        <form method="post">
            <div class="form-group">
                <label>Nama</label>
                <input type="text" name="name" class="form-control"/>
            </div>
            <div class="form-group">
                <label>Email</label>
                <input type="text" name="email" class="form-control"/>
            </div>
            <div class="form-group">
                <label>Password</label>
                <input type="password" name="pass" class="form-control"/>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
<?php $this->load->view('include/footer');?>
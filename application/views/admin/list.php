<?php $this->load->view('include/header');?>
        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Daftar Admin</h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Nama</th>
                      <th>Email</th>
                      <th>Level</th>
                      <th>Tindakan</th>
                    </tr>
                  </thead>
                  <?php foreach($admin as $v){?>
                    <tr>
                      <td><?=$v['name'];?></td>
                      <td><?=$v['email'];?></td>
                      <td style="text-align:center;"><?=$v['level'];?></td>
                      <td><a style="margin-left:9px;" class="btn btn-danger btn-hapus btn-sm btn-circle" href="<?=base_url('admin/hapus/'.$v['id']);?>"><i class="fas fa-trash"></i></></td>
                    </tr>
                  <?php }?>
                  <tfoot>
                    <tr>
                      <th>Nama</th>
                      <th>Email</th>
                      <th>Level</th>
                      <th>Tindakan</th>
                    </tr>
                  </tfoot>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->


<?php $this->load->view('include/footer');?>

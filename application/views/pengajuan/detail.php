<?php $this->load->view('include/header');?>
<link rel="stylesheet" type="text/css" href="<?=base_url('assets/');?>js/bootstrap-tagsinput/bootstrap-tagsinput.css"/>

    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Detail Pengajuan No <?=$pengajuan['no_pengajuan'];?></h1>
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Pemohon</h6>
            </div>
            <div class="card-body">
                <table>
                    <tr>
                        <td>Nama</td>
                        <td>:</td>
                        <td id="user-name"><?=$pengajuan['nama'];?></td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>:</td>
                        <td id="user-email"><?=$pengajuan['email'];?></td>
                    </tr>
                    <tr>
                        <td>Telp</td>
                        <td>:</td>
                        <td id="user-telp"><?=$pengajuan['telp'];?></td>
                    </tr>
                    <tr>
                        <td>No KTP</td>
                        <td>:</td>
                        <td id="user-ktp"><?=$pengajuan['nik'];?></td>
                    </tr>
                    <tr>
                        <td>File KTP</td>
                        <td>:</td>
                        <td id="file-ktp">
                            <a href="<?=base_url('assets/upload/ktp/'.$pengajuan['file_ktp']);?>" id="download-ktp">
                                <img width='250' height='250' src="<?=base_url('assets/upload/ktp/'.$pengajuan['file_ktp']);?>">
                            </a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary" id="pengajuan-date">Pengajuan </h6>
            </div>
            <div class="card-body">
                <table id="table-pengajuan-detail">
                    <tr>
                        <td>Rincian</td>
                        <td>:</td>
                        <td id="pengajuan-rincian"><?=$pengajuan['rincian'];?></td>
                    </tr>
                    <tr>
                        <td>Tujuan</td>
                        <td>:</td>
                        <td id="pengajuan-tujuan"><?=$pengajuan['tujuan'];?></td>
                    </tr>
                    <tr>
                        <td>Cara memperoleh informasi</td>
                        <td>:</td>
                        <td id="pengajuan-peroleh"><?=$pengajuan['cara_peroleh_info'];?></td>
                    </tr>
                    <tr>
                        <td>Cara mendapatkan salinan</td>
                        <td>:</td>
                        <td id="pengajuan-salinan"><?=$pengajuan['cara_dapat_salinan'];?></td>
                    </tr>
                     <tr>
                        <td>Dokumen Pendukung</td>
                        <td>:</td>
                        <td id="file-dokumen">
                            <a href="<?=base_url('assets/upload/doc/'.$pengajuan['file_dokumen']);?>" id="download-dok">
                               Download Dokumen
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>Status</td>
                        <td>:</td>
                        <td id="pengajuan-status"><?=$pengajuan['status'];?></td>
                    </tr>
                    <form action="<?=$action_form;?>" method="post"> 

                    <?php 
                        $csrf = array(
                            'name' => $this->security->get_csrf_token_name(),
                            'hash' => $this->security->get_csrf_hash()
                    );
                    ?>
                    <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                    <tr>
                        <td>Jawaban</td>
                        <td>:</td>
                        <td>
                            <textarea class="form-control" name="jawaban" id="pengajuan-jawaban" cols="70" rows="5" <?=($pengajuan['status']=='Keberatan sedang direview' OR $pengajuan['status']=='Keberatan sudah dijawab')?'disabled':'';?>><?=$pengajuan['jawaban'];?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>Link Lampiran</td>
                        <td>:</td>
                        <td>
                            <input data-role="tagsinput" name="link_lampiran" id="link_lampiran" value="<?=$pengajuan['link_lampiran'];?>" <?=($pengajuan['status']=='Keberatan sedang direview' OR $pengajuan['status']=='Keberatan sudah dijawab')?'disabled':'';?>/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                           <hr style="border-top:1px solid; color:blue; border-top-width:5px;">
                       </td>
                   </tr>
                    
                    <?php if($pengajuan['status']=='Keberatan sedang direview' OR $pengajuan['status']=='Keberatan sudah dijawab'){?>
                    <tr class="keberatan-section">
                        <td>Rincian Keberatan</td>
                        <td>:</td>
                        <td id="keberatan-rincian"><?=$pengajuan['keberatan'];?></td>
                    </tr>
                     <tr class="keberatan-section">
                        <td>Informasi Keberatan</td>
                        <td>:</td>
                        <td id="keberatan-info"><?=$pengajuan['info_keberatan'];?></td>
                    </tr>
                    <tr class="keberatan-section">
                        <td>Alasan Keberatan</td>
                        <td>:</td>
                        <td id="keberatan-alasan"><?=$pengajuan['alasan_keberatan'];?></td>
                    </tr>
                    <tr class="keberatan-section">
                        <td>Jawaban Keberatan</td>
                        <td>:</td>
                        <td>
                        <textarea class="form-control" name="jawaban-keberatan" id="pengajuan-jawaban-keberatan" cols="70" rows="5"><?=$pengajuan['keberatan_jwb'];?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>Link Lampiran</td>
                        <td>:</td>
                        <td>
                            <input data-role="tagsinput" name="link_lampiran_keberatan" id="link_lampiran_keberatan" value="<?=$pengajuan['link_lampiran_keberatan'];?>"/>
                        </td>
                    </tr>
                    <?php }?>
                </table>
                <input type="submit" class="btn btn-primary" value="Update">
                <button style="display:none;" type="button" class="btn btn-primary" id="btn-keberatan">Jawab Keberatan</button>
                </form>
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->

    </div>
    <!-- End of Main Content -->

<?php $this->load->view('include/footer');?>
<script src="<?=base_url('assets/');?>js/bootstrap-tagsinput/bootstrap-tagsinput.js?v=2"></script>
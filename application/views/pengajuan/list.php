<?php $this->load->view('include/header');?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Daftar Pengajuan PPID BP2MI</h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr style="background:#16529b; text-align:center; color:white;">
                      <th style="vertical-align: middle; text-align:center;">No</th>
                      <th style="vertical-align: middle;">Tanggal</th>
                      <th style="vertical-align: middle;">Pemohon</th>
                      <th style=" vertical-align: middle; width:15%;">Cara memperoleh informasi</th>
                      <th style=" vertical-align: middle; width:15%;">Cara mendapatkan informasi</th>
                      <th style="vertical-align: middle;">Rincian permohonan informasi</th>
                      <th style="vertical-align: middle;">Status</th>
                      <th style="vertical-align: middle;">Tindakan</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no=1; foreach ($list_pengajuan as $key => $v) {?>
                      <tr>
                          <td style=" text-align:center;"><?=$no;?></td>
                          <td id="kolom-waktu"><?=$v['waktu'];?></td>
                          <td style="text-align:center;"><?=$v['name']?></td>
                          <td><?=$v['cara_peroleh_info'];?></td>
                          <td><?=$v['cara_dapat_salinan'];?></td>
                          <td><?=$v['rincian'];?></td>
                          <td><strong><?=$v['status'];?></strong></td>
                          <td style="font-weight: bold; color: orange;">
                            <a style="padding:2px; font-size:10px;" class="btn btn-primary btn-sm" href="<?=base_url('pengajuan/detail/'.$v['id']);?>">Detail</a>
                            <a style="margin-left:9px;" class="btn btn-danger btn-hapus btn-sm btn-circle" href="<?=base_url('pengajuan/hapus/'.$v['id']);?>" onclick="return confirm('Anda yakin ingin menghapus?');"><i class="fas fa-trash"></i></a>
                            <?=($v['status']=='sedang direview')?batas_waktu($v['waktu']):'';?>
                          </td>
                          </tr>
                   <?php $no++; } ?>
                  </tbody>
                  <tfoot>
                    <tr style="background:#16529b; text-align:center; color:white;">
                      <th style="vertical-align: middle;">No</th>
                      <th style="vertical-align: middle;">Tanggal</th>
                      <th style="vertical-align: middle; width:25%;">Pemohon</th>
                      <th style=" vertical-align: middle; width:12%;">Cara memperoleh informasi</th>
                      <th style=" vertical-align: middle; width:12%;">Cara mendapatkan informasi</th>
                      <th style="vertical-align: middle;">Rincian permohonan informasi</th>
                      <th style="vertical-align: middle;">Status</th>
                      <th style="vertical-align: middle;">Tindakan</th>
                    </tr>
                  </tfoot>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->


<?php $this->load->view('include/footer');?>
<!-- <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>

<script>
    // $(document).ready( function () {
     var table = $('#dataTable').DataTable();
     //   });
    var db = firebase.database();
    var pengajuanRef = db.ref('Pengajuan2').orderByChild('time').on('value',sukses2,gagal);
    //var pengajuanRef = db.ref('Pengajuan2').on('child_added',sukses2,gagal);
   // var pengajuanRef = db.ref('Pengajuan2').on('child_added',sukses2,gagal);
   

    
    let list_nama = ''
  function sukses(items){
        items.forEach((child) => {
          
          
          
          
          var Users = db.ref('Users/'+child.key);
            Users.once('value', (user)=>{
              list_nama += `<td>${user.val().nama}</td>`;
              //console.log(list_nama);

              //if(list_nama !=''){
                child.forEach(  pengajuan => {
                  //console.log(list_nama);
                  var pengajuan2_tujuan = '';
                  if(pengajuan.val().tujuan) pengajuan2_tujuan = pengajuan.val().tujuan;
                  
                  var pengajuan2_no_pengajuan = '';
                  if(pengajuan.val().no_pengajuan) pengajuan2_no_pengajuan = pengajuan.val().no_pengajuan;
                  
                  var pengajuan2_waktu_jawaban = '';
                  if(pengajuan.val().waktu_jawaban) pengajuan2_waktu_jawaban = pengajuan.val().waktu_jawaban;
                  
                  var pengajuan2_jawaban = '';
                  if(pengajuan.val().jawaban) pengajuan2_jawaban = pengajuan.val().jawaban;
                  
                   var pengajuan2_link_lampiran = '';
                  if(pengajuan.val().link_lampiran) pengajuan2_link_lampiran = pengajuan.val().link_lampiran;
                  
                  var pengajuan2_status = '';
                  if(pengajuan.val().status) pengajuan2_status = pengajuan.val().status;
                  
                  
                  
                  var waktu_bagi = pengajuan.val().waktu.split(" ");
                  switch(waktu_bagi[1]) {
                      case "Mei":
                        var bulan = '5';
                        break;
                      
                      default:
                        var bulan = '6';
                    }
                  pengajuan2_time = bulan+'/'+waktu_bagi[0]+'/'+waktu_bagi[2];
                  
                  db.ref('Pengajuan2/'+pengajuan.key).set({
                          no_pengajuan : pengajuan2_no_pengajuan,
                          selected2 : pengajuan.val().selected2,
                          selected3 : pengajuan.val().selected3,
                          tujuan : pengajuan2_tujuan,
                          waktu : pengajuan.val().waktu,
                          user_id : child.key,
                          user_name : user.val().nama,
                          waktu_jawaban : pengajuan2_waktu_jawaban,
                          jawaban : pengajuan2_jawaban,
                          link_lampiran : pengajuan2_link_lampiran,
                          status : pengajuan2_status,
                          time : pengajuan2_time,
                  });
                  list += `<tr>
                            <td>${pengajuan.val().waktu}</td>
                            ${list_nama}
                            <td>${pengajuan.val().selected2}</td>
                            <td>${pengajuan.val().selected3}</td>
                            <td>${pengajuan.val().tujuan}</td>
                            <td><strong>${pengajuan.val().status}</strong></td>
                            <td><a class="btn btn-primary" href="<?=base_url('pengajuan/detail/');?>${child.key}/${pengajuan.key}">Detail</a></td>
                            </tr>
                          `;
                  $('#dataTable tbody').append(list);
                  list = '';  
                  
                      
                });
                list_nama = '';
              //}
            },err=>{
              console.log('gagl');
            });
          
        });
    }
  function sukses2(items){      
    let list = '';  
    var n = 1;
    var bulan_sekarang;
    var total_notif = 0;
      items.forEach((pengajuan) => {
              
                var bulan = pengajuan.val().time.split("/");

                if(n==1){ 
                  bulan_sekarang = bulan[0];
                }
                else{
                    if(bulan_sekarang != bulan[0]){
                        //list += `<tr><td colspan="8" style="background:red;"></td></tr>`;
                        bulan_sekarang = bulan[0];
                        table.rows.add([[
                        'test',
                        bulan,
                        'test',
                        'test',
                        'test',
                        'test',
                        'test',
                        'test'
                      ]]).draw();
                    }
                }

                table.rows.add([[
                    n,
                    pengajuan.val().waktu,
                    pengajuan.val().user_name,
                    pengajuan.val().selected2,
                    pengajuan.val().selected3,
                    pengajuan.val().tujuan,
                    `<strong>${pengajuan.val().status}</strong>`,
                    `<a class="btn btn-primary" href="<?=base_url('pengajuan/detail/');?>${pengajuan.val().user_id}/${pengajuan.key}">Detail</a>
                      <a class="btn btn-danger btn-hapus" pengajuan-id="${pengajuan.key}" href="#" onclick="hapus()">Hapus</a>`
                  ]]).draw();
                
                
                /*list += `<tr>
                          <td>${n}</td>
                          <td>${pengajuan.val().waktu}</td>
                          <td>${pengajuan.val().user_name}</td>
                          <td>${pengajuan.val().selected2}</td>
                          <td>${pengajuan.val().selected3}</td>
                          <td>${pengajuan.val().tujuan}</td>
                          <td><strong>${pengajuan.val().status}</strong></td>
                          <td><a class="btn btn-primary" href="<?=base_url('pengajuan/detail/');?>${pengajuan.val().user_id}/${pengajuan.key}">Detail</a></td>
                          </tr>
                        `;*/
                        
                
                //$('#dataTable tbody').prepend(list);
                //list = '';  
                n++;
      });
      var no =1;
      for (i = 1; i <= n; i++) {
          if($('#dataTable tbody tr:nth-child('+ i +')').text() !=''){
              $('#dataTable tbody tr:nth-child('+ i +')').find('td:nth-child(1)').text(no);
          }else{
              no =0;
          }
          no++;
      };
        
      //datatable
       
    }
    function gagal(err){
        console.log(err);
  }

    $('body').on('click', '.btn-hapus', function() {
        var r = confirm("Apakah Anda yakin ingin menghapus?");
        var id_pengajuan = $(this).attr('pengajuan-id');
        var pengajuanDeleteRef = db.ref('Pengajuan2/'+id_pengajuan);
        if (r == true) {
          pengajuanDeleteRef.remove();
          alert(`pengajuan ${id_pengajuan} berhasil dihapus.`);
        } else {
          alert('pengajuan gagal dihapus');
        }
    });
    
</script> -->